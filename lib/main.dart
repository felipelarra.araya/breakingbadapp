import 'package:flutter/material.dart';
import 'package:flutterapi/src/pages/character_detalle_page.dart';
import 'package:flutterapi/src/pages/character_page.dart';
import 'package:flutterapi/src/pages/episodes_page.dart';
import 'package:flutterapi/src/pages/frases_page.dart';
import 'package:flutterapi/src/pages/home_page.dart';
import 'package:flutterapi/src/pages/random.page.dart';

 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
      return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Holograms',
      initialRoute: 'home',
      routes: {
        'home'                 : ( BuildContext context ) => HomePage(),
        'characterPage'        : ( BuildContext context ) => CharacterPage(),
        'episodes'             : ( BuildContext context ) => EpisodePage(),
        'detalleCharacter'     : ( BuildContext context ) => CharacterDetalle(),
        'frasesPage'           : ( BuildContext context ) => FrasesPage(),
        'randomPage'           : ( BuildContext context ) => RandomPage()
      }
    );
  }
}
