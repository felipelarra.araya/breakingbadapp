import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutterapi/src/models/character_model.dart';

class CardSwiper extends StatelessWidget {

  final List<CharacterModel> characters;

  CardSwiper({ @required this.characters});
  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;
  
    return  Container(
      child: Swiper(
        layout: SwiperLayout.STACK,
        itemWidth: _screenSize.width * 8.0,
        itemHeight: _screenSize.height * 0.5,
        itemBuilder: (BuildContext context,int index){
          return ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: GestureDetector(
              child: Image(
              image: NetworkImage( characters[index].getPosterImg()) ,
              fit: BoxFit.contain,
            ),
            onTap: (){
               Navigator.pushNamed(context, 'detalleCharacter' ,arguments: characters[index] );
            },
            )
          );
          
        },
        itemCount: characters.length,
      ),
    );

  }
  
}