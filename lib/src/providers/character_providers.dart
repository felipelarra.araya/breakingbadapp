//import 'dart:convert';
import 'package:flutterapi/src/models/character_model.dart';
import 'package:http/http.dart' as  http;

class CharacterProvider{
  final String _url ='https://www.breakingbadapi.com/api';


Future<List<CharacterModel>> cargarCharacter() async {

  http.Response resp = await http.get(_url+'/characters');
  if(resp.statusCode == 200){
  //List<dynamic> body = jsonDecode(resp.body);
  final characterModel  = characterModelFromJson( resp.body );

  //List<CharacterModel> character = body.map((item ) => CharacterModel.fromJson(item)).toList();
  
  return characterModel;
  }
  else{
    throw "No hay personajes";
  }
}

Future<List<CharacterModel>> cargarEpisodes() async {

  http.Response resp = await http.get(_url+'/episodes');
  if(resp.statusCode == 200){
  
  final characterModel  = characterModelFromJson( resp.body );
  
  return characterModel;
  }
  else{
    throw "No hay episodios";
  }
}


}