import 'package:flutterapi/src/models/episode_model.dart';
import 'package:http/http.dart' as  http;

class EpisodeProvider{
  final String _url ='https://www.breakingbadapi.com/api';

Future<List<EpisodeModel>> cargarEpisodes() async {

  http.Response resp = await http.get(_url+'/episodes');
  if(resp.statusCode == 200){
  
  final episodeModel  = episodeModelFromJson( resp.body );
  
  return episodeModel;
  }
  else{
    throw "No hay episodios";
  }
}


}