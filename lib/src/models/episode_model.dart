// To parse this JSON data, do
//
//     final episodeModel = episodeModelFromJson(jsonString);

import 'dart:convert';

List<EpisodeModel> episodeModelFromJson(String str) => List<EpisodeModel>.from(json.decode(str).map((x) => EpisodeModel.fromJson(x)));

String episodeModelToJson(List<EpisodeModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class EpisodeModel {
    int episodeId;
    String title;
    String season;
    String airDate;
    List<String> characters;
    String episode;
    String series;

    EpisodeModel({
        this.episodeId,
        this.title,
        this.season,
        this.airDate,
        this.characters,
        this.episode,
        this.series,
    });

    factory EpisodeModel.fromJson(Map<String, dynamic> json) => EpisodeModel(
        episodeId: json["episode_id"],
        title: json["title"],
        season: json["season"],
        airDate: json["air_date"],
        characters: List<String>.from(json["characters"].map((x) => x)),
        episode: json["episode"],
        series: json["series"],
    );

    Map<String, dynamic> toJson() => {
        "episode_id": episodeId,
        "title": title,
        "season": season,
        "air_date": airDate,
        "characters": List<dynamic>.from(characters.map((x) => x)),
        "episode": episode,
        "series": series,
    };

 }


