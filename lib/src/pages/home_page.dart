import 'dart:ui';

import 'package:flutter/material.dart';
 
void main() => runApp(HomePage());
 
class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _fondoApp(),

          SingleChildScrollView(
            child: Column(
              children: <Widget>[
                _titulos(),
                _botonesRedondeados(context)
              ],
            ),
          )

        ],
      ),
    );
  }

  Widget _fondoApp(){
    final gradiente = Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/fondobb.jpg'),
            fit: BoxFit.cover
            
          )
        ),
  );

    return Stack(
      children: <Widget>[
        gradiente
      ],
    );
  }


  Widget _titulos() {

    return SafeArea(
      child: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('The Breaking Bad', style: TextStyle( color: Colors.white, fontSize: 30.0, fontWeight: FontWeight.bold )),
            SizedBox( height: 10.0 ),
            //Text('Classify this transaction into a particular category', style: TextStyle( color: Colors.white, fontSize: 18.0 )),
          ],
        ),
      ),
    );

  }

  Widget _botonesRedondeados(BuildContext context) {

    return Table(
      children: [
        TableRow(
          children: [
            _crearBotonRedondeado(context, Colors.blue,'assets/walterWhite.jpg', 'Personajes','characterPage' ),
            _crearBotonRedondeado(context, Colors.purpleAccent,'assets/bbIcono.jpg', 'Capítulos','episodes' ),
          ]
        ),
        TableRow(
          children: [
            _crearBotonRedondeado(context, Colors.pinkAccent,'assets/icono3.jpg', 'Frases','frasesPage' ),
            _crearBotonRedondeado(context, Colors.orange,'assets/icono2.jpg', 'Random', 'randomPage' ),
          ]
        ),
      ],
    );

  }

  Widget _crearBotonRedondeado( BuildContext context, Color color, String imagen,  String texto, String ruta ) {
 
 
    return ClipRect(
      child: BackdropFilter(
        filter: ImageFilter.blur( sigmaX: 0.0, sigmaY: 0.0 ),
        child: Container(
          height: 180.0,
          margin: EdgeInsets.all(50.0),
          decoration: BoxDecoration(
            //color: Color.fromRGBO(20, 20, 20, 2),
            borderRadius: BorderRadius.circular(20.0)
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              SizedBox( height: 5.0 ),
              InkWell(
                onTap: () => Navigator.pushNamed(context, ruta),
                child: CircleAvatar(
                backgroundImage: AssetImage(imagen),
                backgroundColor: color,
                radius: 40.0,
                //child: Icon( icono, color: Colors.white, size: 30.0 ),
              ),
              ),
              Text( texto , style: TextStyle( color: color )),
              SizedBox( height: 5.0 )
            ],
          ),
        ),
      ),
    );
  }
}