import 'package:flutter/material.dart';
import 'package:flutterapi/src/models/episode_model.dart';
import 'package:flutterapi/src/providers/episode_providers.dart';
 
void main() => runApp(EpisodePage());
 
class EpisodePage extends StatelessWidget {

  final episodeProvider = new EpisodeProvider();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
    body: _crearListado2(),
    
    );
     
  }


  Widget _crearListado2(){
    
    return FutureBuilder(
      future: episodeProvider.cargarEpisodes(),
      builder: (BuildContext context, AsyncSnapshot<List<EpisodeModel>> snapshot) {
        if ( snapshot.hasData ){
          final episodes = snapshot.data;
          return ListView.builder(
            itemCount: episodes.length,
            itemBuilder: (context,i ) => _crearItem2(context , episodes[i]),
          );
        }else{
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

Widget _crearItem2( BuildContext context, EpisodeModel episode){
  return Container(

    child: Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      child: Column(
        children: <Widget>[
          ListTile(
          leading: CircleAvatar(
          backgroundImage: AssetImage('assets/icono2.jpg'),
          ),  
          title: Text('Nombre: '+'${episode.title }'),
          subtitle: Text('Temporada ' + '${episode.season}'),

          //onTap: () => Navigator.pushNamed(context, 'detalleProducto',arguments: producto),
          ),
        ],
      ),
    ),
  );
}



}