
import 'package:flutter/material.dart';
import 'package:flutterapi/src/models/character_model.dart';
 
void main() => runApp(CharacterDetalle());
 
class CharacterDetalle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
  final CharacterModel character = ModalRoute.of(context).settings.arguments;
    return Scaffold(
        body: CustomScrollView(
          slivers: <Widget>[
            _crearAppbar( character ),
            SliverList(
              delegate: SliverChildListDelegate(
                [
                SizedBox(height: 10.0,),
                _descripcion(character),
                _ocupacion(character),
                _actor(character),
                _status(character)
                ]
              ),
            )
          ],
        )
    );
  }


  Widget _crearAppbar( CharacterModel character ){

    return SliverAppBar(
      elevation: 2.0,
      backgroundColor: Colors.black,
      expandedHeight: 250.0,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text(
          character.name,
          style: TextStyle(color: Colors.white, fontSize: 16.0),
        ),
        background: FadeInImage(
          image: NetworkImage( character.getPosterImg() ),
          placeholder: AssetImage('assets/loading.gif'),
          fadeInDuration: Duration(microseconds: 150),
          fit: BoxFit.contain,
        ),
      ),
    );

  }

    Widget _descripcion( CharacterModel character ) {

    return Container(

      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
      child: Text(
        character.nickname,
        textAlign: TextAlign.justify,
        style: TextStyle(fontSize: 30.0 , fontStyle: FontStyle.italic),
      ),
    );

  }

    Widget _ocupacion( CharacterModel character ) {

    return Container(

      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
      child: Text(
        'Ocupación : ' + character.occupation.toString(),
        textAlign: TextAlign.justify,
        
      ),
    );

  }

 Widget _actor( CharacterModel character ) {

    return Container(

      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),  
      child: Text(
        'Actor : ' + character.portrayed,
        textAlign: TextAlign.left,
        
      ),
    );

  }


 Widget _status( CharacterModel character ) {

    return Container(

      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
      child: Text(
        'Status : ' + character.status,
        textAlign: TextAlign.left,
        
      ),
    );

  }


}