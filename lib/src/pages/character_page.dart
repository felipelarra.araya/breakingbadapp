import 'package:flutter/material.dart';
import 'package:flutterapi/src/models/character_model.dart';
import 'package:flutterapi/src/providers/character_providers.dart';
import 'package:flutterapi/src/widgets/card_swiper_widget.dart';
 
void main() => runApp(CharacterPage());
 
class CharacterPage extends StatelessWidget {
  final characterProvider  = new CharacterProvider();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
    body: Stack(
      children: <Widget>[
        _fondo(),
        _swiperTarjetas()
      ],
    )
              //_swiperTarjetas(),

        );
    
  }

Widget _fondo(){
  return Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/fondobb.jpg'),
            fit: BoxFit.cover
            
          )
        ),
  );
}

Widget _crearListado2(){
    
    return FutureBuilder(
      future: characterProvider.cargarCharacter(),
      builder: (BuildContext context, AsyncSnapshot<List<CharacterModel>> snapshot) {
        if ( snapshot.hasData ){
          final productos = snapshot.data;
          return ListView.builder(
            itemCount: productos.length,
            itemBuilder: (context,i ) => _crearItem2(context , productos[i]),
          );
        }else{
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

   Widget _swiperTarjetas(){

    return FutureBuilder(
      future: characterProvider.cargarCharacter(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {

        if(snapshot.hasData){
          return CardSwiper(characters: snapshot.data);
          
        }

        else{
          return Container(
            height: 400.0,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ); 
        }
       
      },
    );
    }

 Widget _crearItem2( BuildContext context, CharacterModel characterModel){
  return Dismissible(
    key: UniqueKey(),
    background: Container(
      color: Colors.red,
    ),
    onDismissed: ( direccion ){
      //productosProvider.borrarProducto(producto.id);
    },
    child: Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      child: Column(
        children: <Widget>[
          FadeInImage(
            image: NetworkImage(characterModel.img ),
            placeholder: AssetImage('assets/jar-loading.gif'),
            height: 300.0,
            width: double.infinity,
            fit: BoxFit.cover,
          ),
          ListTile(
          title: Text('${characterModel.name }'),
          //subtitle: Text('${producto.descripcion}'),
          //onTap: () => Navigator.pushNamed(context, 'detalleProducto',arguments: producto),
          ),
        ],
      ),
    ),
  );
}
  
}
